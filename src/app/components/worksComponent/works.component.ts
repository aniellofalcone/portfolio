import { Component } from '@angular/core';

@Component({
    selector: 'works-section',
    templateUrl: './works.component.html',
    styleUrls:  ['./works.component.css']

})

export class WorksComponent {}
