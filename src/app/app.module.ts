                      //*****     ANGULAR COMPONENTS     *****
import { BrowserModule }            from '@angular/platform-browser';
import { NgModule, enableProdMode } from '@angular/core';
import { FormsModule }              from '@angular/forms';
import { HttpModule }               from '@angular/http';
import {
  MaterialModule,
  MdInputModule,
  MdSidenavModule,
  MdToolbarModule,
  MdMenuModule }                    from '@angular/material';
import { BrowserAnimationsModule }  from '@angular/platform-browser/animations';
import { } from '';

                    //***** AGM (GOOGLE MAPS) COMPONENTS *****
import {
  AgmCoreModule,
  AgmMarker,
  AgmInfoWindow }                   from '@agm/core';

                    //*****     BOOTSRAP COMPONENTS      *****
import { CarouselModule }           from 'ngx-bootstrap/carousel';
import { PopoverModule }            from 'ngx-bootstrap/popover';

                    //*****     PERSONAL COMPONENTS      *****
import { NavComponent }             from './components/navbarComponent/navbar.component';
import { IntroComponent }           from './components/introContent/intro.component';
import { AboutComponent }           from './components/aboutComponent/about.component';
import { WorksComponent }           from './components/worksComponent/works.component';
import { ContactComponent }         from './components/contactComponent/contact.component';
import { MapComponent }             from './components/mapContent/map.component';
import { FooterComponent }          from './components/footerComponent/footer.component';

import 'hammerjs';

import { AppComponent }             from './components/app.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    IntroComponent,
    AboutComponent,
    WorksComponent,
    ContactComponent,
    MapComponent,
    FooterComponent,
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MaterialModule,
    CarouselModule.forRoot(),
    PopoverModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBVnIg2h96t_3pVuqjOwH5YUqf5due0t90'
    })
  ],

  entryComponents: [],
  providers: [],
  bootstrap: [AppComponent, ContactComponent]
})

export class AppModule {}
