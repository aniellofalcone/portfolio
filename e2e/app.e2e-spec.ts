import { AnielloFalconePortfolio } from './app.po';

describe('aniello-falcone-portfolio App', () => {
  let page: AnielloFalconePortfolio;

  beforeEach(() => {
    page = new AnielloFalconePortfolio();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
