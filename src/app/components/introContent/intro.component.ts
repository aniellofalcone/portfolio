import { Component } from '@angular/core';
import './intro.component.css';

@Component({
    selector    : 'intro-section',
    templateUrl : './intro.component.html',
    styleUrls   :  ['./intro.component.css'],

})

export class IntroComponent {
  quotes                          = [
    {
      content : '"Stay hungry, stay foolish!"',
      author  : "Steve Jobs"
    },
    {
      content : '"There are two ways to write error-free programs; only the third one works."',
      author  : "Alan J. Perlis"
    },
    {
      content : '"Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live."',
      author  : "Martin Golding"
    },
    {
      content : '"Programming today is a race between software engineers striving to build bigger and better idiot-proof programs, and the universe trying to produce bigger and better idiots. So far, the universe is winning."',
      author  : "Rick Cook"
    },
    {
      content : '"Most good programmers do programming not because they expect to get paid or get adulation by the public, but because it is fun to program."',
      author  : "Linus Torvald"
    },
    {
      content : '"Sometimes it pays to stay in bed on Monday, rather than spending the rest of the week debugging Monday\'s code."',
      author  : "Christopher Thompson"
    }
  ];
  interval              : number  = 4000;
}
