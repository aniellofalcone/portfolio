import { Component } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
    selector: 'contact-section',
    templateUrl: './contact.component.html',
    styleUrls:  ['./contact.component.css'],

  animations: [trigger(
  'openClose',
  [
    state('out, void', style({height: '0px', visibility:'hidden'})),
    state('in', style({height: '*', color: 'green', visibility: 'visible'})),
    transition(
        'in <=> out', [animate(500, style({height: '250px', visibility: 'visible'})), animate(500)])
  ])],
  })

export class ContactComponent {
    linkedinUrl : string  = "https://www.linkedin.com/in/aniello-falcone-42aa85114/";
    facebookUrl : string  = "https://www.facebook.com/aniello.falcone.50";
    twitterUrl  : string  = "https://twitter.com/AnielloFalcone1";

    stateFinal: string = '';
    stateExpression: boolean = true;
    constructor() { this.expand() }

    expand() {
        this.stateExpression = !this.stateExpression;

        if(this.stateExpression){
          this.stateFinal = 'in';
        } else {
          this.stateFinal = 'out';
        }
    }
}
