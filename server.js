var express = require('express');
// create a new express server
var app = express(),
    path = require('path'),
    bodyParser = require('body-parser');

app.use(bodyParser.json({
    limit: '5mb'
}));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(express.static(__dirname + '/dist'));

app.set('port', (process.env.PORT || 8080));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    return res.status(404).send();
});

// start server on the specified port and binding host
app.listen(app.get('port'), function () {
    console.log("server started");
});
