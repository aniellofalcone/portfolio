webpackJsonp([1],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("../../../material/@angular/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/@angular/platform-browser/animations.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_bootstrap_carousel__ = __webpack_require__("../../../../ngx-bootstrap/carousel/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_popover__ = __webpack_require__("../../../../ngx-bootstrap/popover/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_navbarComponent_navbar_component__ = __webpack_require__("../../../../../src/app/components/navbarComponent/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_introContent_intro_component__ = __webpack_require__("../../../../../src/app/components/introContent/intro.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_aboutComponent_about_component__ = __webpack_require__("../../../../../src/app/components/aboutComponent/about.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_worksComponent_works_component__ = __webpack_require__("../../../../../src/app/components/worksComponent/works.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_contactComponent_contact_component__ = __webpack_require__("../../../../../src/app/components/contactComponent/contact.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_mapContent_map_component__ = __webpack_require__("../../../../../src/app/components/mapContent/map.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_footerComponent_footer_component__ = __webpack_require__("../../../../../src/app/components/footerComponent/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_hammerjs__ = __webpack_require__("../../../../hammerjs/hammer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_hammerjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_app_component__ = __webpack_require__("../../../../../src/app/components/app.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
//*****     ANGULAR COMPONENTS     *****






//***** AGM (GOOGLE MAPS) COMPONENTS *****

//*****     BOOTSRAP COMPONENTS      *****


//*****     PERSONAL COMPONENTS      *****









var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_17__components_app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_9__components_navbarComponent_navbar_component__["a" /* NavComponent */],
            __WEBPACK_IMPORTED_MODULE_10__components_introContent_intro_component__["a" /* IntroComponent */],
            __WEBPACK_IMPORTED_MODULE_11__components_aboutComponent_about_component__["a" /* AboutComponent */],
            __WEBPACK_IMPORTED_MODULE_12__components_worksComponent_works_component__["a" /* WorksComponent */],
            __WEBPACK_IMPORTED_MODULE_13__components_contactComponent_contact_component__["a" /* ContactComponent */],
            __WEBPACK_IMPORTED_MODULE_14__components_mapContent_map_component__["a" /* MapComponent */],
            __WEBPACK_IMPORTED_MODULE_15__components_footerComponent_footer_component__["a" /* FooterComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["a" /* MaterialModule */],
            __WEBPACK_IMPORTED_MODULE_7_ngx_bootstrap_carousel__["a" /* CarouselModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_popover__["a" /* PopoverModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_6__agm_core__["a" /* AgmCoreModule */].forRoot({
                apiKey: 'AIzaSyCpyynxOiIpugN8SGU5lW_cYuCNiZYIKbU'
            })
        ],
        entryComponents: [],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_17__components_app_component__["a" /* AppComponent */], __WEBPACK_IMPORTED_MODULE_13__components_contactComponent_contact_component__["a" /* ContactComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/components/aboutComponent/about.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*********** GENERAL SETTINGS ***********/\n\n.content-section {\n  padding-top: 100px;\n  padding-bottom: 100px;\n}\n\n/*********** MOBILE SETTINGS ***********/\n\n@media (min-width: 768px) {\n  .content-section {\n    padding-top: 250px;\n  }\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/aboutComponent/about.component.html":
/***/ (function(module, exports) {

module.exports = "<section id=\"about\" class=\"container content-section text-center\">\n  <div class=\"row\">\n    <div class=\"col-lg-8 col-lg-offset-2\">\n      <h2>About me</h2>\n    </div>\n  </div>\n</section>\n"

/***/ }),

/***/ "../../../../../src/app/components/aboutComponent/about.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AboutComponent = (function () {
    function AboutComponent() {
    }
    return AboutComponent;
}());
AboutComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["l" /* Component */])({
        selector: 'about-section',
        template: __webpack_require__("../../../../../src/app/components/aboutComponent/about.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/aboutComponent/about.component.css")]
    })
], AboutComponent);

//# sourceMappingURL=about.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/app.component.html":
/***/ (function(module, exports) {

module.exports = "<body id=\"page-top\">\n\n    <!-- Navbar Section  -->\n    <scroll-navbar id=\"scroll-navbar\"></scroll-navbar>\n\n    <!-- Intro Section -->\n    <intro-section id=\"intro-section\"></intro-section>\n\n    <!-- About Section -->\n    <about-section id=\"about-section\"></about-section>\n\n    <!-- Works Section -->\n    <works-section id=\"works-section\"></works-section>\n\n    <!-- Contact Section -->\n    <contact-section id=\"contact-section\"></contact-section>\n\n    <!-- Map Section -->\n    <map-section id=\"map-section\"></map-section>\n\n    <!-- Footer Section  -->\n    <footer-section id=\"footer-section\"></footer-section>\n\n</body>\n"

/***/ }),

/***/ "../../../../../src/app/components/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'Aniello Falcone Dev Portfolio';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["l" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/components/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/contactComponent/contact.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*********** GENERAL SETTINGS ***********/\n\n.contact-section {\n  padding-top: 50px;\n}\n.contact-section a:hover{\n  text-decoration: underline;\n}\n.toggle-container {\n  position: absolute;\n  background-color: white;\n  border:10px solid black;\n  width:200px;\n  text-align:center;\n  line-height:100px;\n  font-size:50px;\n  box-sizing:border-box;\n  overflow:hidden;\n  z-index: 10;\n  visibility: hidden;\n}\nul.banner-social-buttons {\n  margin-bottom: 35px;\n}\n\n/*********** MOBILE SETTINGS ***********/\n\n@media (max-width: 767px) {\n  .list-inline {\n    display: -webkit-inline-box;\n    display: -ms-inline-flexbox;\n    display: inline-flex;\n    margin-bottom: 34px;\n\n  }\n  ul.banner-social-buttons li {\n    display: block;\n\n    margin: 15px;\n    padding: 0;\n  }\n\n  ul.banner-social-buttons li span {\n    display: none;\n  }\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/contactComponent/contact.component.html":
/***/ (function(module, exports) {

module.exports = "<section id=\"contact\" class=\"container text-center contact-section\" >\n  <div class=\"row\">\n    <div class=\"col-lg-8 col-lg-offset-2\">\n      <h2>Contacts</h2>\n      <p>Feel free to email me, give me suggestions, or to just say hello!</p>\n      <p><a href=\"mailto:aniellofalcone92@gmail.com\">aniellofalcone92@gmail.com</a>\n      </p>\n\n      <ul class=\"list-inline banner-social-buttons\">\n        <li>\n          <a id=\"twitter\" [href]=\"twitterUrl\" class=\"btn btn-default btn-lg\">\n            <i class=\"fa fa-twitter fa-fw\"></i>\n              <span class=\"network-name\">Twitter</span>\n          </a>\n        </li>\n\n        <li>\n          <a id=\"github\" [href]=\"facebookUrl\" class=\"btn btn-default btn-lg\">\n            <i class=\"fa fa-facebook fa-fw\"></i>\n             <span class=\"network-name\">Facebook</span>\n          </a>\n        </li>\n\n        <li>\n          <a id=\"google+\" [href]=\"linkedinUrl\" class=\"btn btn-default btn-lg\">\n            <i class=\"fa fa-linkedin fa-fw\"></i>\n             <span class=\"network-name\">Linkedin</span>\n          </a>\n        </li>\n\n      </ul>\n\n    </div>\n    <!-- <div class=\"\">\n      <button (mouseover)=\"expand()\" (mouseleave)=\"expand()\">Open</button>\n      <button (click)=\"collapse()\">Closed</button>\n      <div class=\"toggle-container\" [@openClose]=\"stateFinal\">\n        Look at this box\n      </div>\n    </div> -->\n  </div>\n</section>\n"

/***/ }),

/***/ "../../../../../src/app/components/contactComponent/contact.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_animations__ = __webpack_require__("../../../animations/@angular/animations.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactComponent = (function () {
    function ContactComponent() {
        this.linkedinUrl = "https://www.linkedin.com/in/aniello-falcone-42aa85114/";
        this.facebookUrl = "https://www.facebook.com/aniello.falcone.50";
        this.twitterUrl = "https://twitter.com/AnielloFalcone1";
        this.stateFinal = '';
        this.stateExpression = true;
        this.expand();
    }
    ContactComponent.prototype.expand = function () {
        this.stateExpression = !this.stateExpression;
        if (this.stateExpression) {
            this.stateFinal = 'in';
        }
        else {
            this.stateFinal = 'out';
        }
    };
    return ContactComponent;
}());
ContactComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["l" /* Component */])({
        selector: 'contact-section',
        template: __webpack_require__("../../../../../src/app/components/contactComponent/contact.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/contactComponent/contact.component.css")],
        animations: [__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["a" /* trigger */])('openClose', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["b" /* state */])('out, void', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["c" /* style */])({ height: '0px', visibility: 'hidden' })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["b" /* state */])('in', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["c" /* style */])({ height: '*', color: 'green', visibility: 'visible' })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["d" /* transition */])('in <=> out', [__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["e" /* animate */])(500, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["c" /* style */])({ height: '250px', visibility: 'visible' })), __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["e" /* animate */])(500)])
            ])],
    }),
    __metadata("design:paramtypes", [])
], ContactComponent);

//# sourceMappingURL=contact.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/footerComponent/footer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*********** GENERAL SETTINGS ***********/\n\nfooter {\n  padding: 50px 0;\n}\nfooter p {\n  margin: 0;\n  font-size: 16px;\n}\n#signature {\n  font-family: Snell Roundhand;\n  font-size: 25px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/footerComponent/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer>\n  <div class=\"container text-center\">\n    <p>Made with <i class=\"fa fa-code\" aria-hidden=\"true\"></i> and  <i class=\"fa fa-spotify\" aria-hidden=\"true\"></i></p>\n    <p>by</p>\n    <p id=\"signature\">Aniello Falcone</p>\n  </div>\n</footer>\n"

/***/ }),

/***/ "../../../../../src/app/components/footerComponent/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FooterComponent = (function () {
    function FooterComponent() {
    }
    return FooterComponent;
}());
FooterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["l" /* Component */])({
        selector: 'footer-section',
        template: __webpack_require__("../../../../../src/app/components/footerComponent/footer.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/footerComponent/footer.component.css")]
    })
], FooterComponent);

//# sourceMappingURL=footer.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/introContent/intro.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*********** GENERAL SETTINGS ***********/\n\n:host >>> .carousel-intro {\n  min-width: 100%;\n  margin-right: 0;\n  margin-left: 0;\n}\n:host >>> .carousel-indicators {\n  bottom: -70px;\n\n  display: none !important;\n}\n:host >>> .carousel-control {\n  display: none !important;\n}\n:host >>> .carousel-inner {\n  position: relative;\n\n  overflow: hidden;\n\n  width: 80%;\n  height: 160px;\n  margin-right: auto;\n  margin-left: auto;\n}\n:host >>> #author {\n  position: absolute;\n  right: 15vw;\n\n  font-family: cursive;\n  font-weight: bold;\n}\n.intro .intro-body .brand-heading {\n  font-size: 100px;\n}\n.intro .intro-body .intro-text {\n  font-size: 26px;\n}\n.intro {\n  display: table;\n\n  width: 100%;\n  height: 100%;\n  padding: 100px 0;\n  padding-top: 15vh !important;\n  padding-bottom: 15vh !important;\n\n  text-align: center;\n\n  color: white;\n  background: url(/assets/img/intro-bg.jpg) no-repeat bottom center scroll;\n  background-color: black;\n  background-size: cover;\n}\n.intro .intro-body {\n  display: table-cell;\n  vertical-align: middle;\n}\n.intro .intro-body .brand-heading {\n  font-size: 40px;\n}\n.intro .intro-body .intro-text {\n  font-size: 18px;\n}\n\n/*********** MOBILE SETTINGS ***********/\n\n@media (max-width: 767px) {\n  .intro .intro-body {\n    padding-top: 0;\n  }\n  :host >>> .carousel-inner {\n    height: 330px;\n    padding-top: 50px;\n  }\n  .btn-circle {\n    margin-top: 10vh;\n  }\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/introContent/intro.component.html":
/***/ (function(module, exports) {

module.exports = "<header class=\"intro\">\n  <div class=\"intro-body\">\n    <div class=\"container carousel-intro\">\n      <div class=\"row\">\n        <div>\n          <carousel [interval]=\"interval\">\n            <slide *ngFor=\"let quote of quotes;\">\n              <p id=\"quote\">\n                {{quote.content}}\n              </p>\n              <p id=\"author\">\n                - {{quote.author}}\n              </p>\n            </slide>\n          </carousel>\n        </div>\n      </div>\n\n    </div>\n    <a href=\"#about\">\n      <div class=\"scroll-down\">\n        <span>\n          <i class=\"fa fa-angle-down fa-2x\"></i>\n        </span>\n      </div>\n    </a>\n  </div>\n</header>\n"

/***/ }),

/***/ "../../../../../src/app/components/introContent/intro.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__intro_component_css__ = __webpack_require__("../../../../../src/app/components/introContent/intro.component.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__intro_component_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__intro_component_css__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IntroComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var IntroComponent = (function () {
    function IntroComponent() {
        this.quotes = [
            {
                content: '"Stay hungry, stay foolish!"',
                author: "Steve Jobs"
            },
            {
                content: '"There are two ways to write error-free programs; only the third one works."',
                author: "Alan J. Perlis"
            },
            {
                content: '"Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live."',
                author: "Martin Golding"
            },
            {
                content: '"Programming today is a race between software engineers striving to build bigger and better idiot-proof programs, and the universe trying to produce bigger and better idiots. So far, the universe is winning."',
                author: "Rick Cook"
            },
            {
                content: '"Most good programmers do programming not because they expect to get paid or get adulation by the public, but because it is fun to program."',
                author: "Linus Torvald"
            },
            {
                content: '"Sometimes it pays to stay in bed on Monday, rather than spending the rest of the week debugging Monday\'s code."',
                author: "Christopher Thompson"
            }
        ];
        this.interval = 4000;
    }
    return IntroComponent;
}());
IntroComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["l" /* Component */])({
        selector: 'intro-section',
        template: __webpack_require__("../../../../../src/app/components/introContent/intro.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/introContent/intro.component.css")],
    })
], IntroComponent);

//# sourceMappingURL=intro.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/mapContent/map.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*********** GENERAL SETTINGS ***********/\n\n#map {\n  width: 80%;\n  margin: auto;\n}\n.sebm-google-map-container {\n  height: 300px;\n}\n:host >>> .agm-info-window-content {\n  color: black !important;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/mapContent/map.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"map\">\n  <agm-map [latitude]=\"lat\" [longitude]=\"lng\" [zoom]=\"zoom\" [mapDraggable]=\"mapDraggable\"\n   [zoomControl]=\"mapDraggable\" [disableDoubleClickZoom]=\"disableDoubleClickZoom\"\n   [scrollwheel]=\"scrollwheel\" [streetViewControl]=\"streetViewControl\" [styles]=\"styles\">\n\n    <agm-marker [latitude]=\"lat\" [longitude]=\"lng\" [iconUrl]=\"iconUrl\"\n    [openInfoWindow]=\"openInfoWindow\" [opacity]=\"opacity\">\n\n      <agm-info-window isOpen=\"true\">\n        <strong>Viale Monza, 13</strong>\n        <br/>20125 Milano\n      </agm-info-window>\n      \n    </agm-marker>\n  </agm-map>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/mapContent/map.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MapComponent = (function () {
    function MapComponent() {
        this.lat = 45.488150;
        this.lng = 9.217067;
        this.zoom = 15;
        this.mapDraggable = false;
        this.zoomControl = false;
        this.disableDoubleClickZoom = false;
        this.scrollwheel = false;
        this.streetViewControl = false;
        this.keyboardShortcuts = false;
        this.iconUrl = "./assets/img/map-marker.png";
        this.openInfoWindow = true;
        this.opacity = 0.6;
        this.styles = [
            {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative.province",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    },
                    {
                        "saturation": "-100"
                    },
                    {
                        "lightness": "30"
                    }
                ]
            },
            {
                "featureType": "administrative.neighborhood",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    },
                    {
                        "gamma": "0.00"
                    },
                    {
                        "lightness": "74"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "all",
                "stylers": [
                    {
                        "lightness": "3"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "weight": "1.00"
                    },
                    {
                        "lightness": "0"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    }
                ]
            }
        ];
    }
    return MapComponent;
}());
MapComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["l" /* Component */])({
        selector: 'map-section',
        template: __webpack_require__("../../../../../src/app/components/mapContent/map.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/mapContent/map.component.css")]
    })
], MapComponent);

//# sourceMappingURL=map.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/navbarComponent/navbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*********** GENERAL SETTINGS ***********/\n\n.navbar-custom.top-nav-collapse {\n  padding: 0;\n\n  border-bottom: 1px solid rgba(255, 255, 255, 0.3);\n  background: black;\n}\n.navbar-custom{\n  margin-bottom: 0;\n  padding: 20px 0;\n  transition: all 1s ease-out;\n  letter-spacing: 1px;\n  text-transform: uppercase;\n\n  border-bottom: none;\n  background: transparent;\n\n  font-family: \"Montserrat\", \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n}\n.scrolled-nav {\n  max-height: 50px !important;\n  padding: 0 !important;\n\n  background-color: black !important;\n}\n.navbar-custom .navbar-toggle {\n  display: none;\n\n  color: white;\n  background-color: rgba(255, 255, 255, 0.2);\n\n  font-size: 12px;\n}\n.navbar-custom .navbar-toggle:focus,\n.navbar-custom .navbar-toggle:active {\n  outline: none;\n}\n.navbar-custom .navbar-brand {\n  font-weight: 700;\n}\n.navbar-custom .navbar-brand:focus {\n  outline: none;\n}\n.navbar-custom a {\n  color: white;\n}\n.navbar-custom .nav li a {\n  transition: background 0.3s ease-in-out;\n}\n.navbar-custom .nav li a:hover {\n  color: #76FF03;\n  background-color: transparent;\n  outline: none;\n  text-decoration: underline;\n}\n.navbar-custom .nav li a:focus,\n.navbar-custom .nav li a:active {\n  color: #76FF03;\n  background-color: rgba(255, 255, 255, 0.1);\n  outline: none;\n  background-color: transparent;\n}\n.navbar-custom .nav li.active {\n  outline: none;\n}\n.navbar-custom .nav li.active a {\n  background-color: rgba(255, 255, 255, 0.1);\n}\n.navbar-custom .nav li.active a:hover {\n  color: white;\n}\n.clicked-btn {\n  color: #76FF03 !important;\n}\n\n\n/*********** MOBILE SETTINGS ***********/\n\n@media (max-width: 767px) {\n  .navbar-custom {\n    padding: 0;\n  }\n  .navbar-toggle {\n    display: block !important;\n\n    width: 50px !important;\n    height: 50px !important;\n  }\n  .navbar-custom .navbar-toggle {\n    transition: background-color 1s ease-out;\n\n    background-color: transparent;\n\n    font-size: 0;\n  }\n  .navbar-brand {\n    font-size: 13px;\n  }\n  .navbar-fixed-top {\n    z-index: 1000;\n  }\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/navbarComponent/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-custom navbar-fixed-top\" [ngClass]= \"{'scrolled-nav': isScrolled}\" role=\"navigation\">\n  <div class=\"container\">\n    <div class=\"navbar-header\">\n      <button md-icon-button [mdMenuTriggerFor]=\"menu\" class=\"navbar-toggle\"  yPosition=\"below\">\n        <md-icon>more_vert</md-icon>\n      </button>\n      <md-menu #menu=\"mdMenu\" style=\"background-color: black; color: white;\">\n        <a md-menu-item href=\"#about\">\n          <span>About</span>\n        </a>\n        <a md-menu-item href=\"#works\">\n          <span>Works</span>\n        </a>\n        <a md-menu-item href=\"#contact\">\n          <span>Contact</span>\n        </a>\n      </md-menu>\n      <a class=\"navbar-brand \" href=\"#page-top\" id=\"pageTopBtn\" (click)=\"clickedColor($event)\">\n        <i class=\"fa fa-code-fork\"></i> <span class=\"light\" id=\"pageTopBtn\" (click)=\"clickedColor($event)\" [ngClass]= \"{'clicked-btn': stateExpressionObject.pageTopBtn}\" style=\"text-decoration: none\">Aniello</span> Falcone\n      </a>\n    </div>\n\n    <div class=\"collapse navbar-collapse navbar-right navbar-main-collapse\">\n      <ul class=\"nav navbar-nav\">\n        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->\n        <li class=\"hidden\">\n          <a href=\"#page-top\"></a>\n        </li>\n        <li>\n          <a href=\"#about\" id=\"aboutBtn\" (click)=\"clickedColor($event)\" [ngClass]= \"{'clicked-btn': stateExpressionObject.aboutBtn}\">About</a>\n        </li>\n        <li>\n          <a href=\"#works\" id=\"worksBtn\" (click)=\"clickedColor($event)\" [ngClass]= \"{'clicked-btn': stateExpressionObject.worksBtn}\">Works</a>\n        </li>\n        <li>\n          <a href=\"#contact\" id=\"contactBtn\" (click)=\"clickedColor($event)\" [ngClass]= \"{'clicked-btn': stateExpressionObject.contactBtn}\">Contact</a>\n        </li>\n      </ul>\n    </div>\n  </div>\n</nav>\n"

/***/ }),

/***/ "../../../../../src/app/components/navbarComponent/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavComponent; });
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavComponent = (function () {
    function NavComponent() {
        this.isScrolled = false;
        this.currPos = 0;
        this.startPos = 0;
        this.changePos = 100;
        this.stateExpressionObject = {
            'aboutBtn': false,
            'worksBtn': false,
            'contactBtn': false,
            'pageTopBtn': false
        };
        this.backupObject = __assign({}, this.stateExpressionObject);
    }
    NavComponent.prototype.updateHeader = function (evt) {
        this.currPos = (window.pageYOffset || evt.target.scrollTop) - (evt.target.clientTop || 0);
        if (this.currPos >= this.changePos) {
            this.isScrolled = true;
        }
        else {
            this.isScrolled = false;
        }
    };
    NavComponent.prototype.clickedColor = function (event) {
        var currentId = "";
        var clickedId = event.srcElement.attributes.id.nodeValue;
        this.stateExpressionObject = __assign({}, this.backupObject);
        this.stateExpressionObject[clickedId] = !this.stateExpressionObject[clickedId];
    };
    return NavComponent;
}());
NavComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["l" /* Component */])({
        selector: 'scroll-navbar',
        template: __webpack_require__("../../../../../src/app/components/navbarComponent/navbar.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/navbarComponent/navbar.component.css")],
        host: {
            '(window:scroll)': 'updateHeader($event)'
        }
    }),
    __metadata("design:paramtypes", [])
], NavComponent);

//# sourceMappingURL=navbar.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/worksComponent/works.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*********** GENERAL SETTINGS ***********/\n\n.works-section {\n  width: 100%;\n  padding: 100px 0;\n\n  color: white;\n  background: url(/assets/img/downloads-bg.jpg) no-repeat center center scroll;\n  background-color: black;\n  background-size: cover;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/worksComponent/works.component.html":
/***/ (function(module, exports) {

module.exports = "<section id=\"works\" class=\"content-section text-center\">\n  <div class=\"works-section\">\n    <div class=\"container\">\n      <div class=\"col-lg-8 col-lg-offset-2\">\n        <h2>Gallery</h2>\n        <p>Theese are some of my best coded works</p>\n        <a href=\"http://startbootstrap.com/template-overviews/grayscale/\" class=\"btn btn-default btn-lg\">Visit works Page</a>\n      </div>\n    </div>\n  </div>\n</section>\n"

/***/ }),

/***/ "../../../../../src/app/components/worksComponent/works.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WorksComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var WorksComponent = (function () {
    function WorksComponent() {
    }
    return WorksComponent;
}());
WorksComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["l" /* Component */])({
        selector: 'works-section',
        template: __webpack_require__("../../../../../src/app/components/worksComponent/works.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/worksComponent/works.component.css")]
    })
], WorksComponent);

//# sourceMappingURL=works.component.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map